package sandbox.wrkup.adapters.html

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.ResponseBody
import sandbox.wrkup.domain.model.KupConfig
import sandbox.wrkup.domain.model.WorkdayPattern
import sandbox.wrkup.domain.services.generate
import java.time.LocalDate

@Controller
class Endpoints {

    @GetMapping("/")
    @ResponseBody
    fun home() = homeHtml(LocalDate.now().withDayOfMonth(1))

    @PostMapping("/report")
    @ResponseBody
    fun report(req: ReportRequest) : String {
        LoggerFactory.getLogger(Endpoints::class.java).info("Generating report for {}", req)
        return try {
            val report = generate(
                KupConfig.parse(req.config),
                WorkdayPattern.parse(req.pattern),
                req.absenceDays.parseInts(),
                req.reportMonth
            )
            """
                <html><body>
                <ul>
                <li>Copy the below text</li>
                <li>Select cell A10 in KUP spreadsheet</li>
                <li>Paste</li>
                <li>Click Data -> Split text to columns</li>
                <li>Select Separator:Semicolon in the popup</li>
                </ul>
                <textarea rows="30" wrap = "off" style="width:100%;">$report</textarea>
                </body></html>
            """.trimIndent()
        } catch (e: Exception) {
            "Oops... ${e.message}"
        }
    }
}

private fun homeHtml(defaultMonth: LocalDate) = """
<html>
    <body>
    <form action="/report" method="post">
        <label for="config">KUP config. Must match what you see in dropdowns of KUP spreadsheet. You can change values if they are outdated:</label><br/>
        <textarea id="config" name="config" rows="18" wrap = "off" style="width:100%;">${KupConfig.DefaultYaml}</textarea><br/>
        <br/>
        <label for="pattern">Your workday pattern. You can update from/to, projectName, jiraNumber. And you can add more activities, as long as they match keys in config above:</label><br/>
        <textarea id="pattern" name="pattern" rows="9" wrap = "off" style="width:100%;">${WorkdayPattern.Sample}</textarea><br/>
        <br/>
        <label for="absenceDays">Your days off or sick. Comma-separated, e.g. 4,5,6,27,28,31</label><br/>
        <input type="text" id="absenceDays" name="absenceDays" style="width:100%;"/><br/>
        <br/>
        <center>
            <label for="reportMonth">Report month:</label>
            <input type="date" id="reportMonth" name="reportMonth" value="$defaultMonth"></input>
            <input type="submit" value="Generate report"></input>
        </center>
    </form>
    </body>
</html>
"""

data class ReportRequest(val config: String, val pattern: String, val absenceDays: String, val reportMonth: LocalDate)

private fun String.parseInts() : List<Int> {
    if(trim().isEmpty()) return emptyList()
    return split(",").fold(mutableListOf()) { list, item ->
        list.add(Integer.parseInt(item.trim()))
        list
    }
}
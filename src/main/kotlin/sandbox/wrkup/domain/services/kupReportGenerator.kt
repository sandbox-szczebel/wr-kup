package sandbox.wrkup.domain.services

import sandbox.wrkup.domain.model.KupConfig
import sandbox.wrkup.domain.model.WorkdayPattern
import java.io.PrintWriter
import java.io.StringWriter
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter

private val dd_slash_mm_slash_yyyy = DateTimeFormatter.ofPattern("dd/MM/yyyy")

fun generate(
    cfg: KupConfig,
    pattern: WorkdayPattern,
    absenceDays: List<Int>,
    month:LocalDate = LocalDate.now(),
    separator: Char = ';') : String {

    val capture = StringWriter()
    val out = PrintWriter(capture)

    forEachDayOfMonth(month) { day ->
        val date = day.format(dd_slash_mm_slash_yyyy)

        if(day.dayOfWeek == DayOfWeek.SATURDAY || day.dayOfWeek == DayOfWeek.SUNDAY)
            out.println("$date$separator${cfg.weekendDayTypeLabel}$separator${cfg.weekendActivityLabel}")
        else if(absenceDays.contains(day.dayOfMonth))
            out.println("$date$separator${cfg.absenceDayTypeLabel}$separator${cfg.absenceActivityLabel}")
        else pattern.activities.forEach {
            val activityLabel = cfg.workActivities.getOrElse(it.activity) {"unknown activity : " + it.activity}
            out.println("$date$separator${cfg.workingDayTypeLabel}$separator${activityLabel}$separator${it.from}$separator${it.to}$separator${it.projectName}$separator${it.jiraNumber}")
        }
    }
    return capture.toString().trim()
}

private fun forEachDayOfMonth(month: LocalDate, consumer: (LocalDate) -> Unit) {
    val start = month.withDayOfMonth(1)
    start.datesUntil(start.plusMonths(1)).forEach(consumer)
}
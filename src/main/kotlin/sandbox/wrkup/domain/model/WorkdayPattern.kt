package sandbox.wrkup.domain.model

import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.introspector.BeanAccess

data class WorkdayPattern(
    val activities: List<WorkdayActivity> = emptyList()
) {
    companion object {
        private val parser = Yaml().apply { setBeanAccess(BeanAccess.FIELD) }

        fun parse(yaml: String): WorkdayPattern = parser.loadAs(yaml, WorkdayPattern::class.java)

        const val Sample = """
activities:
- activity: admin
  from: "09:00"
  to: "12:00"
- activity: coding
  from: "12:00"
  to: "17:00"
  projectName: "xxx"
  jiraNumber: "zzz"
"""
    }
}

data class WorkdayActivity(
    val activity: String = "",
    val from: String = "",
    val to: String = "",
    val projectName: String = "",
    val jiraNumber: String = "",
)
package sandbox.wrkup.domain.model

import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.introspector.BeanAccess

data class KupConfig(
    val workingDayTypeLabel : String = "",
    val weekendDayTypeLabel : String = "",
    val weekendActivityLabel : String = "",
    val absenceDayTypeLabel : String = "",
    val absenceActivityLabel : String = "",
    val workActivities: Map<String, String> = emptyMap()
) {
    companion object {
        private val parser = Yaml().apply { setBeanAccess(BeanAccess.FIELD) }

        fun parse(yaml: String) : KupConfig = parser.loadAs(yaml, KupConfig::class.java)

        const val DefaultYaml = """
workingDayTypeLabel: "Working day"
weekendDayTypeLabel: "Weekend/bank holiday"
weekendActivityLabel: "Absence/weekend"
absenceDayTypeLabel: "Absence - holiday/sick leave"
absenceActivityLabel: "Absence/weekend"
workActivities:
    admin: "Administrative work"
    coding: "Coding and performing code and architecture reviews"
    test: "Write automated unit tests to ensure high quality features are delivered"
    tdd: "Continuously improving the code with TDD-writing TDD tests"
    docs: "Creating Documentation of IT software and architecture"
    docs_manager: "Documentation of tools and processes: feasibility studies, analysis and design, data models, as-built documentation, technical operation and procedures, maintenance manuals commited in documentation repositories (Confluence pages, Google Docs and Slides)"
    improve: "Improving efficiency and quality through patterns, processes and tooling"
    frontend: "Converting visual designs to semantic markup, then dress it up in CSS and then bring it to life with JavaScript"
    perf_tuning: "Optimizing application for maximum speed"
    seo: "SEO optimisation and application security"
    contentful: "Developing and maintaining Contentful model structuring"
    user-facing: "Developing new user-facing features"
    dev: "Creating and maintaining software and software documentation"
    sdlc: "SDLC improvement proposals and progress raports committed to documentation repositories"
    materials: "Creating training materials committed to documentation repositories"
    process: "Creating policy, processes, procedures documentation"
"""
    }
}


package sandbox.wrkup.domain.services

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import sandbox.wrkup.domain.model.KupConfig
import sandbox.wrkup.domain.model.WorkdayPattern
import java.time.LocalDate
import java.time.Month

class KupReportGeneratorTests {

    @Test
    fun `report generation for defaults works`() {

        val actual = generate(
            KupConfig.parse(KupConfig.DefaultYaml),
            WorkdayPattern.parse(WorkdayPattern.Sample),
            listOf(1, 2),
            LocalDate.of(2021, Month.NOVEMBER, 15)
        )

//        println("Expected:")
//        println(nov2021template)
//        println("Actual:")
//        println(actual.replace("\r", ""))
//        println("---")

        assertThat(actual.replace("\r", ""))
            .isEqualTo(nov2021report)
    }
}

private val nov2021report = """
01/11/2021;Absence - holiday/sick leave;Absence/weekend
02/11/2021;Absence - holiday/sick leave;Absence/weekend
03/11/2021;Working day;Administrative work;09:00;12:00;;
03/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
04/11/2021;Working day;Administrative work;09:00;12:00;;
04/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
05/11/2021;Working day;Administrative work;09:00;12:00;;
05/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
06/11/2021;Weekend/bank holiday;Absence/weekend
07/11/2021;Weekend/bank holiday;Absence/weekend
08/11/2021;Working day;Administrative work;09:00;12:00;;
08/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
09/11/2021;Working day;Administrative work;09:00;12:00;;
09/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
10/11/2021;Working day;Administrative work;09:00;12:00;;
10/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
11/11/2021;Working day;Administrative work;09:00;12:00;;
11/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
12/11/2021;Working day;Administrative work;09:00;12:00;;
12/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
13/11/2021;Weekend/bank holiday;Absence/weekend
14/11/2021;Weekend/bank holiday;Absence/weekend
15/11/2021;Working day;Administrative work;09:00;12:00;;
15/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
16/11/2021;Working day;Administrative work;09:00;12:00;;
16/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
17/11/2021;Working day;Administrative work;09:00;12:00;;
17/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
18/11/2021;Working day;Administrative work;09:00;12:00;;
18/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
19/11/2021;Working day;Administrative work;09:00;12:00;;
19/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
20/11/2021;Weekend/bank holiday;Absence/weekend
21/11/2021;Weekend/bank holiday;Absence/weekend
22/11/2021;Working day;Administrative work;09:00;12:00;;
22/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
23/11/2021;Working day;Administrative work;09:00;12:00;;
23/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
24/11/2021;Working day;Administrative work;09:00;12:00;;
24/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
25/11/2021;Working day;Administrative work;09:00;12:00;;
25/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
26/11/2021;Working day;Administrative work;09:00;12:00;;
26/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
27/11/2021;Weekend/bank holiday;Absence/weekend
28/11/2021;Weekend/bank holiday;Absence/weekend
29/11/2021;Working day;Administrative work;09:00;12:00;;
29/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
30/11/2021;Working day;Administrative work;09:00;12:00;;
30/11/2021;Working day;Coding and performing code and architecture reviews;12:00;17:00;xxx;zzz
        """.trimIndent().trim()
package sandbox.wrkup.domain.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class KupConfigTests {

    @Test
    fun `default KupConfig should parse properly`() {

        val expected = KupConfig(
            "Working day",
            "Weekend/bank holiday",
            "Absence/weekend",
            "Absence - holiday/sick leave",
            "Absence/weekend",
            mapOf(
                Pair("admin",       "Administrative work"),
                Pair("coding",      "Coding and performing code and architecture reviews"),
                Pair("test",        "Write automated unit tests to ensure high quality features are delivered"),
                Pair("tdd",         "Continuously improving the code with TDD-writing TDD tests"),
                Pair("docs",        "Creating Documentation of IT software and architecture"),
                Pair("docs_manager","Documentation of tools and processes: feasibility studies, analysis and design, data models, as-built documentation, technical operation and procedures, maintenance manuals commited in documentation repositories (Confluence pages, Google Docs and Slides)"),
                Pair("improve",     "Improving efficiency and quality through patterns, processes and tooling"),
                Pair("frontend",    "Converting visual designs to semantic markup, then dress it up in CSS and then bring it to life with JavaScript"),
                Pair("perf_tuning", "Optimizing application for maximum speed"),
                Pair("seo",         "SEO optimisation and application security"),
                Pair("contentful",  "Developing and maintaining Contentful model structuring"),
                Pair("user-facing", "Developing new user-facing features"),
                Pair("dev",         "Creating and maintaining software and software documentation"),
                Pair("sdlc",        "SDLC improvement proposals and progress raports committed to documentation repositories"),
                Pair("materials",   "Creating training materials committed to documentation repositories"),
                Pair("process",     "Creating policy, processes, procedures documentation"),
            )
        )

        assertThat(KupConfig.parse(KupConfig.DefaultYaml)).isEqualTo(expected)
    }
}
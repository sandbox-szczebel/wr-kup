package sandbox.wrkup.domain.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class WorkdayPatternTests {

    @Test
    fun `sample WorkdayPattern should parse properly`() {

        val expected = WorkdayPattern(listOf(
            WorkdayActivity("admin", "09:00", "12:00", "", ""),
            WorkdayActivity("coding", "12:00", "17:00", "xxx", "zzz"),
        ))

        assertThat(WorkdayPattern.parse(WorkdayPattern.Sample).activities)
            .hasSameElementsAs(expected.activities)
    }

    @Test
    fun `sample WorkdayPattern not in conflict with default KupConfig`() {
        val kupCfg = KupConfig.parse(KupConfig.DefaultYaml)
        val sample = WorkdayPattern.parse(WorkdayPattern.Sample)

        sample.activities.forEach {
            assertThat(kupCfg.workActivities.keys).contains(it.activity)
        }
    }
}
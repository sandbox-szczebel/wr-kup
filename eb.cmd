echo off
docker run -i --rm -w /work -v %cd%:/work -e AWS_ACCESS_KEY_ID=%AWS_ACCESS_KEY% -e AWS_SECRET_ACCESS_KEY=%AWS_SECRET_KEY% coxauto/aws-ebcli eb %*